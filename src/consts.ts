// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = 'Race Events';
export const SITE_DESCRIPTION = 'See a log of mountain bike races with details and results';
