---
name: 'Epic 8 Hour (Hardwood Hills)'
description: Summer and Fall events
promoter: Pulse Racing
website: https://pulseracing.ca/events/
region: CA-ON
location: https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2844.8773695830273!2d-79.59432862290072!3d44.51766517107421!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882aa78e805ad2e3%3A0xf9c6c5579005cec7!2sHardwood%20Ski%20and%20Bike!5e0!3m2!1sen!2sus!4v1719750962030!5m2!1sen!2sus
results:
  - date: 2023-09-23
    link: https://zone4.ca/race/2023-09-23/96f5489c/results
    file: fall-epic-8-2023.csv
  - date: 2022-09-24
    link: https://zone4.ca/race/2022-09-24/57c741d2/results
    file: fall-epic-8-2022.csv
---

### Accommodations

[Best Western Plus (Barrie, ON)](https://maps.app.goo.gl/1BELguTY6WPGkVkc8)
