---
name: Raccon Rally
description: a bike race
promoter: HeartRateUp
website: https://heartrateup.com/
region: NY
location: https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5918.286523352556!2d-78.7062963!3d42.1258162!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d29beb15555555%3A0xacd430641eeac08d!2sArt%20Roscoe%20Ski%20Area!5e0!3m2!1sen!2sus!4v1720100081613!5m2!1sen!2sus
---

get rad on double track
