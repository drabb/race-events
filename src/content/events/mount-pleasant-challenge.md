---
name: Mount Pleasant Challenge
description: a bike race
promoter: Mount Pleasant
website: https://www.skimountpleasant.com/Challenge
region: PA
location: https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d380409.6595117247!2d-80.070502!3d41.850893!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88328a60b56ee24d%3A0x78230bacf9d77b9b!2s14510%20Mt%20Pleasant%20Rd%2C%20Cambridge%20Springs%2C%20PA%2016403!5e0!3m2!1sen!2sus!4v1720184922959!5m2!1sen!2sus
---
