---
name: Wilmington Whiteface MTB
description: a bike race
promoter: Adirondack Sports Council
website: https://www.wilmingtonmtb.com
region: NY
results:
  - date: 2022-06-04
    link: http://www.iresultslive.com/?op=overall&eid=5278
    file: whiteface-mountain-2022.csv
---

whiteface mtb marathon details here
