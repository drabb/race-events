---
name: Sprague Brook Wed Night Series
description: a bike race
promoter: Campus Wheelworks
website: http://campuscyclingcollective.com/
region: NY
location: https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d2937.309101607748!2d-78.64552492269539!3d42.59118497117234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zNDLCsDM1JzI4LjMiTiA3OMKwMzgnMzQuNiJX!5e0!3m2!1sen!2sus!4v1720103396741!5m2!1sen!2sus
results:
  - date: 2022-06-04
    link: http://www.iresultslive.com/?op=overall&eid=5278
    file: whiteface-mountain-2022.csv
---

bruh

#### 2024 breakdown ([series details](https://www.webscorer.com/seriesresult?seriesid=352895&dist=Singe%20Speed&gender=O))

| Race      | Placement  | Time Behind |
| ------ | ------- | ---- |
| Race 1, 2024-05-15 | DNF | – |
| Race 2, 2024-06-12 | (2/3) | +3:30 |
