import { defineCollection, z } from 'astro:content';

const blog = defineCollection({
	type: 'content',
	// Type-check frontmatter using a schema
	schema: z.object({
		title: z.string(),
		description: z.string(),
		// Transform string to Date object
		pubDate: z.coerce.date(),
		updatedDate: z.coerce.date().optional(),
		heroImage: z.string().optional(),
	}),
});

const results = z.array(
	z.object({
		date: z.coerce.date(),
		link: z.string().optional(),
		file: z.string().optional(),
		notes: z.string().optional(),  // "499/500 entrants" "fried rim at mile 3"
	}),
);

const events = defineCollection({
	type: 'content',
	schema: z.object({
		name: z.string(),
		description: z.string().optional(),
		website: z.string().optional(),
		promoter: z.string().optional(),
		results: results.optional(),
		region: z.enum(['NY', 'PA', 'CA-ON']).optional(),
		location: z.string().optional(), // street address or coordinate pair or map url
		// list of dates attended
		// attachments or links
		// photos
	})
})

export const collections = { blog, events };
