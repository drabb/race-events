.PHONY: build deploy build-n-deploy

build:
	npx astro build

deploy:
	s3cmd --no-mime-magic --acl-public --delete-removed --delete-after --no-check-certificate sync ./dist/ s3://race-events

build-n-deploy: build deploy

